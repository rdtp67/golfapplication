//Programer: rdt
//File: User.h
//Purpose: User Class

#ifndef USER_H
#define USER_H
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "control.h"





using namespace std;



class User : public control
{
    public:

        //Purpose: Allows user to enter a new user name
        //Post: Adds new user to txt file
        void enterUser();

        //Purpose: Prints out list of users
        //Post: Prints
        void printUser();

        //Purpose: Gets the original set of users from text file
        //Post: Sets the user list
        void getUser();

        //Purpose: Deletes a specified User
        //Post: Deletes User from vector
        void deleteUser();

        //Purpose: Shows the User Menu
        //Post: Returns true to leave menu false to stay
        void user_menu();



    private:

        vector<string> username;

    protected:

};
#include "User.hpp"
#endif

