//Programer: rdt
//File: course.h
//Purpose: course class

#ifndef COURSE_H
#define COURSE_H
#define HOLEAMOUNT 18
#include <iostream>
#include <string>
#include <vector>
#include "control.h"

using namespace std;

struct course_hole{

        int par;
        int yardage;


};

struct idv_course{

        course_hole hole[HOLEAMOUNT];
        string course_name;

};

class course : public control
{

    private:

        vector<idv_course> course_list;

    public:

        //Purpose: sets course name
        //Post: none
        void setCourse_name();

        //Purpose: sets hole
        //Post: none
        void setHole(int hole_num);

        //Purpose: counts amount of holes in a course
        //Post: returns amount of holes
        int count_holes(string name);

        //Purpose: adds up yardage
        //Post: returns yardage
        int add_yardage(string name);

        //Purpose: gives average yaradge per hole of course
        //Post: returns average yardage
        int avg_yardage(string name);

        //Purpose: course menu
        //Post: none
        void course_menu();

        //Purpose: prints course list
        //Post: none
        void print_course_list();

        //Purpose: delete course
        //Post: none
        void delete_course(string name);

        //Purpose: gets course list from file
        //Post: none
        void getCourse_list();




};

#endif


