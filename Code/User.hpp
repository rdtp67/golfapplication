//Programer: rdt
//File: User.hpp
//Purpose: User class functions

#ifndef USER_HPP
#define USER_HPP
using namespace std;



void User:: enterUser()
{

    string username;
    string newline = "\n";
    char user_check;
    bool user_ansr = false;
    ofstream outputFile;
    outputFile.open("user.txt", std::ios_base::app);

    while (user_ansr == false)
    {
    cout << "Enter User Name: ";
    cin >> username;
    cout << "Is " << username << " correct? (Y/N)" << endl;
    cin >> user_check;
    if(user_check == 'Y' || user_check == 'y')
    {
        outputFile << username;
        outputFile << newline;
        user_ansr = true;

    }
    else
    {
        system("CLS");
        cout << "Try Again? (Y/N) " << endl;
        cin >> user_check;
        if(user_check == 'Y' || user_check == 'y')
        {
         user_ansr = false;
        }
        else
        {
         user_ansr = true;
        }
    }
    }

    outputFile.close();
    return;
}

void User:: printUser()
{

    getUser();
    ifstream inputFile;
    inputFile.open("user.txt");

    system("CLS");

    for( std::vector<string>::const_iterator i = username.begin(); i != username.end(); ++i)
    {
        std::cout << *i << endl;
    }

    inputFile.close();
    return;
}

void User:: getUser()
{

    username.clear();


    string line;
    ifstream inputFile;
    inputFile.open("user.txt");

    while( !inputFile.eof())
    {
        getline(inputFile, line);
        username.push_back(line);
    }

    inputFile.close();


}

void User:: deleteUser()
{
    string User_to_delete;
    int delete_index=0;
    ofstream outputFile;


    system("CLS");
    printUser();
    cout << "Enter the name of the user you wish to delete?" << endl;
    cin >> User_to_delete;

    for(int i=0; i<username.size()-1;i++)
    {
        if(username.at(i) == User_to_delete)
        {
            username.erase(username.begin()+i);
        }
    }

    outputFile.open("user.txt", ios::trunc);
    while(delete_index < username.size()-1)
    {
     outputFile << username.at(delete_index);
     outputFile << "\n";
     delete_index++;
    }

            outputFile.close();

    printUser();


    return;
}

void User:: user_menu()
{

    char user_choice;
    bool close_menu = false;

    //Main Menu
    while(close_menu == false)
    {
     cout << "**************************************************" << endl;
     cout << "********         User Menu        ****************" << endl;
     cout << "********                          ****************" << endl;
     cout << "********   1. Enter User          ****************" << endl;
     cout << "********   2. Print User List     ****************" << endl;
     cout << "********   3. Delete User         ****************" << endl;
     cout << "********   4. Close Menu          ****************" << endl;
     cout << "********                          ****************" << endl;
     cout << "**************************************************" << endl;
     cin >> user_choice;

     switch(user_choice)
     {
         case '1': enterUser();
            break;
         case '2': printUser();
            break;
        case '3': deleteUser();
            break;
        default: close_menu = exit_menu();
     }
    }

    return;

}


#endif

